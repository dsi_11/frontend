type Material = {
    id?: number
    name: string
    price: number
    qty: number
    min: number
    unit: string
  }
  export { type Material }