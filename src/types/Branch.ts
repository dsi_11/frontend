type Branch = {
    id?: number
    name: string
    address: string
    tel: string
    zipcode: string
}
export { type Branch }