type Promotion = {
  id?: number
  name: string
  discount: number
  usePoint: number
  limit?: number
}
export { type Promotion }
