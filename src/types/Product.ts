
type Product = {
  id?: number;
  name: string;
  price: number;
  type: 'Drink' | 'Bakery' | 'Food';
  subCategory?: 'Cold' | 'Hot' | 'Frappe' | '-';
}
function getImageUrl(product: Product) {
  return `/img/coffees/product${product.id}.png`
}
export { type Product, getImageUrl }
