import type { Bill } from "@/types/Bill";
import http from "./http";

function addUser(bill: Bill) {
    return http.post('/bills', bill)
}

function updateUser(bill: Bill) {
    return http.patch(`/bills/${bill.id}`, bill)
}

function delUser(bill: Bill) {
    return http.delete(`/bills/${bill.id}`)
}

function getUser(id: number) {
    return http.delete(`/bills/${id}`)
}

function getUsers() {
    return http.get('/bills/')
}

export default { addUser, updateUser, delUser, getUser,getUsers }
