import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import materialService from '@/services/material'
import type { Material } from '@/types/Material'

export const useMaterialStore = defineStore('material', () => {
    const loadingStore = useLoadingStore()
    const materials = ref<Material[]>([])
    const initialMaterial: Material = {
        
        name: '',
        price: 0,
        qty: 0,
        min: 0,
        unit: ''
      }
    
      const editedMaterial = ref<Material>(JSON.parse(JSON.stringify(initialMaterial)))

    async function getMaterial(id: number) {
        loadingStore.doLoad()
        const res = await materialService.getMaterial(id)
        materials.value = res.data
        loadingStore.finish()
    }
    async function getMaterials() {
        loadingStore.doLoad()
        const res = await materialService.getMaterials()
        materials.value = res.data
        loadingStore.finish()
    }
    async function saveMaterial() {
        loadingStore.doLoad()
        const material = editedMaterial.value
        if (!material.id) {
            //Add new
            console.log('Post ' + JSON.stringify(material))
            const res = await materialService.addMaterial(material)
        } else {
            //Update
            console.log('Patch ' + JSON.stringify(material))
            const res = await materialService.updateMaterial(material)
        }
        await getMaterials()
        loadingStore.finish()
    }
    async function deleteMaterial() {
        loadingStore.doLoad()
        const material = editedMaterial.value
        const res = await materialService.delMaterial(material)
        // clearForm()
        await getMaterials()
        loadingStore.finish()
      }
    

    function clearForm() {
        editedMaterial.value = JSON.parse(JSON.stringify(initialMaterial))
      }
    return { materials, getMaterial, getMaterials, saveMaterial, deleteMaterial, clearForm, editedMaterial}
})