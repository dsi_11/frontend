import { defineStore } from 'pinia'
import { ref } from 'vue'
import productService from '@/services/product'
import type { Product } from '@/types/Product'
import { useLoadingStore } from './loading'
import product from '@/services/product'

export const useProductStore = defineStore('Product', () => {
  const loadingStore = useLoadingStore()
  const products = ref<Product[]>([])
  const initialProduct: Product = {
    // id: -1,
    name: '',
    price: 0,
    type: 'Drink',
    subCategory: 'Cold'
  }

  const editedProduct = ref<Product>(JSON.parse(JSON.stringify(initialProduct)))
  //data function
  function getSubcategories(type: string) {
    if (type === 'Bakery' || type === 'Food') {
      return ['-']
    } else {
      return ['Cold', 'Hot', 'Frappe']
    }
  }

  async function getProduct(id: number) {
    loadingStore.doLoad()
    const res = await productService.getProduct(id)
    editedProduct.value = res.data
    loadingStore.finish()
  }

  async function getProducts() {
    loadingStore.doLoad()
    const res = await productService.getProducts()
    products.value = res.data
    loadingStore.finish()
  }
  async function saveProduct() {
    loadingStore.doLoad()
    const product = editedProduct.value
    if (!product.id) {
      //Add new
      console.log('Post ' + JSON.stringify(product))
      const res = await productService.addProduct(product)
    } else {
      //update
      console.log('Patch ' + JSON.stringify(product))
      const res = await productService.updateProduct(product)
    }
    // clearForm()
    await getProducts()
    loadingStore.finish()
  }
  async function deleteProduct() {
    loadingStore.doLoad()
    const product = editedProduct.value
    const res = await productService.delProduct(product)
    // clearForm()
    await getProducts()
    loadingStore.finish()
  }

  function clearForm() {
    editedProduct.value = JSON.parse(JSON.stringify(initialProduct))
  }

  return {
    getSubcategories,
    products,
    getProduct,
    getProducts,
    saveProduct,
    deleteProduct,
    editedProduct,
    clearForm
  }
})
