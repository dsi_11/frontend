import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { Branch } from '@/types/Branch'
import branchService from '@/services/branch'
import { useLoadingStore } from './loading'

export const useBranchStore = defineStore('branch', () => {
    const loadingStore = useLoadingStore()
    const Branches = ref<Branch[]>([])
    const initialBranch: Branch = {
        name: '',
        address: '',
        tel: '',
        zipcode: '',
    }
    const editedBranch = ref<Branch>(JSON.parse(JSON.stringify(initialBranch)))

    async function getBranch(id: number) {
        loadingStore.doLoad()
        const res = await branchService.getBranch(id)
        editedBranch.value = res.data
        loadingStore.finish()
    }
    async function getBranches() {
        loadingStore.doLoad()
        const res = await branchService.getBranches()
        Branches.value = res.data
        loadingStore.finish()
    }
    async function saveBranch() {
        loadingStore.doLoad()
        const branch = editedBranch.value
        if (!branch.id) {//Add new
            console.log('Post ' + JSON.stringify(branch))
            const res = await branchService.addBranch(branch)
        } else {//Update
            console.log('Patch ' + JSON.stringify(branch))
            const res = await branchService.updateBranch(branch)
        }

        await getBranches()
        loadingStore.finish()
    }
    async function deleteBranch() {
        loadingStore.doLoad()
        const branch = editedBranch.value
        const res = await branchService.delBranch(branch)
        await getBranches()
        loadingStore.finish()
    }

    function clearForm() {
        editedBranch.value = JSON.parse(JSON.stringify(initialBranch))
    }
    return { Branches, getBranches, saveBranch, deleteBranch, editedBranch, getBranch, clearForm }
})
