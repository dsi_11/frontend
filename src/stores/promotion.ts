import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import promotionService from '@/services/promotion'
import type { Promotion } from '@/types/Promotion'

export const usePromotionStore = defineStore('promotion', () => {
  const loadingStore = useLoadingStore()
  const promotions = ref<Promotion[]>([])
  const initialPromotion: Promotion = {
    // id: -1,
    name: '',
    discount: 0,
    usePoint: 0
  }
  const editedPromotion = ref<Promotion>(JSON.parse(JSON.stringify(initialPromotion)))

  async function getPromotions() {
    loadingStore.doLoad()
    const res = await promotionService.getPromotions()
    promotions.value = res.data
    loadingStore.finish()
  }

  async function getPromotion(id: number) {
    loadingStore.doLoad()
    const res = await promotionService.getPromotion(id)
    editedPromotion.value = res.data
    loadingStore.finish()
  }

  async function savePromotion() {
    loadingStore.doLoad()
    const promotion = editedPromotion.value
    if (!promotion.id) {
      //Add new
      console.log('Post ' + JSON.stringify(promotion))
      const res = await promotionService.addPromotion(promotion)
    } else {
      //update
      console.log('Patch ' + JSON.stringify(promotion))
      const res = await promotionService.updatePromotion(promotion)
    }
    // clearForm()
    await getPromotions()
    loadingStore.finish()
  }

  async function deletePromotion() {
    loadingStore.doLoad()
    const promotion = editedPromotion.value
    const res = await promotionService.delPromotion(promotion)
    // clearForm()
    await getPromotions()
    loadingStore.finish()
  }

  function clearForm() {
    editedPromotion.value = JSON.parse(JSON.stringify(initialPromotion))
  }

  return {
    getPromotions,
    promotions,
    getPromotion,
    savePromotion,
    deletePromotion,
    clearForm,
    editedPromotion
  }
})
