import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import userService from '@/services/bill'
import type { Bill } from '@/types/Bill'

export const useBillStore = defineStore('bill', () => {
    const loadingStore = useLoadingStore()
    const users = ref<Bill[]>([])
    async function getUser(id: number) {
        loadingStore.doLoad()
        const res = await userService.getUser(id)
        users.value = res.data
        loadingStore.finish()
    }
    async function getUsers() {
        loadingStore.doLoad()
        const res = await userService.getUsers()
        users.value = res.data
        loadingStore.finish()
    }
    async function saveUser(user: Bill) {
        loadingStore.doLoad()
        if (user.id < 0) {
            //Add new
            console.log('Post ' + JSON.stringify(user))
            const res = await userService.addUser(user)
        } else {
            //Update
            console.log('Patch ' + JSON.stringify(user))
            const res = await userService.updateUser(user)
        }
        await getUsers()
        loadingStore.finish()
    }
    async function deleteUser(user: Bill) {
        loadingStore.doLoad()
        const res = await userService.delUser(user)
        await getUsers()
        loadingStore.finish()
    }
    return { users, getUsers, saveUser, deleteUser }
})
